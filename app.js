var http        = require('http'),
    forwardings = require('./redirects');

var Handlers = {
  "Proxy": function(res, host, url, parameters) {
    // Find request forwardings
    if (typeof forwardings.proxy[host] != "undefined" &&
        typeof forwardings.proxy[host][url] != "undefined") {
      var target = forwardings.proxy[host][url];

      delete parameters.headers.host;

      var options = {
        "host": target.host,
        "port": target.port,
        "path": target.path,
        "method": parameters.method,
        "headers": parameters.headers
      };

      if (parameters.get.length > 0) {
        if (options.path.match(/\?/)) {
          options.path += "&" + parameters.get;
        } else {
          options.path += "?" + parameters.get;
        }
      }

      var subRequest = http.request(options, function(subResult) {
        subResult.setEncoding('utf8');
        subResult.on('data', function (chunk) {
          res.write(chunk);
        });
        subResult.on('end', function (chunk) {
          return res.end();
        });
      });

      if (parameters.post.length > 0) {
        subRequest.write(parameters.post);
      }

      subRequest.end();
      return true;
    }
    return false;
  },
  "Path": function(res, host, url, parameters) {
    // Find path under requested domain
    if (typeof forwardings.url[host] != 'undefined') {
      if (typeof forwardings.url[host][url] != 'undefined') {
        res.writeHead(302, { 'Location': forwardings.url[host][url] });
        return res.end();
      }
    }
    return false;
  },
  "WildDomain": function(req, host, url, parameters) {
    // Find path under wildcard-domain
    if (typeof forwardings.url['*'] != 'undefined') {
      if (typeof forwardings.url['*'][url] != 'undefined') {
        res.writeHead(302, { 'Location': forwardings.url['*'][url] });
        return res.end();
      }
    }
    return false;
  },
  "Domain": function(res, host, url, parameters) {
    // Find full domain redirect
    if (typeof forwardings.domain[host] != 'undefined') {
      res.writeHead(302, { 'Location': forwardings.domain[host] });
      return res.end();
    }
    return false;
  }
};

http.createServer(function (req, res) {
  var hostname   = req.headers.host.replace(/:.*/, ""),
      url        = req.url,
      location   = null,
      parameters = {
        get: "",
        post: "",
        method: req.method,
        headers: {}
      };

  if (url.match(/\?/)) {
    _parts = url.split(/\?/);
    url = _parts[0];
    parameters.get = _parts[1];
  } else {
    // no get parameters
  }

  parameters.headers = req.headers;

  //if (parameters.method == 'POST') {
    req.on('data', function(chunk) {
      parameters.post += chunk.toString();
    });

    req.on('end', function() {
      for (var name in Handlers) {
        if (Handlers.hasOwnProperty(name)) {
          if (Handlers[name].call(Handlers, res, hostname, url, parameters)) {
            return true;
          }
        }
      }

      res.writeHead(404);
      return res.end("Hostname -> URL does not found!");
    });
  //}
}).listen(process.env.PORT || 5000);